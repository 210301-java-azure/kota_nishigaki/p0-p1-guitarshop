package dev.kota;

public class Driver {
    public static void main(String[] args) {

        App app = new App();
        app.start(Integer.parseInt(System.getenv("JAVALIN_PORT")));

    }
}
