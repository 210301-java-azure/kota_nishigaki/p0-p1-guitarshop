package dev.kota.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "cart")
@IdClass(CartId.class)
public class Cart implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Id
    @ManyToOne
    @JoinColumn(name = "guitar_id", referencedColumnName = "id")
    private Guitar guitar;

    private int quantity;

    public Cart() {super();}

    public Cart(User user, Guitar guitar, int quantity) {
        this.user = user;
        this.guitar = guitar;
        this.quantity = quantity;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Guitar getGuitar() {
        return guitar;
    }

    public void setGuitar(Guitar guitar) {
        this.guitar = guitar;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cart cart = (Cart) o;
        return quantity == cart.quantity && Objects.equals(user, cart.user) && Objects.equals(guitar, cart.guitar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, guitar, quantity);
    }

    @Override
    public String toString() {
        return "Cart{" +
                "user=" + user +
                ", guitar=" + guitar +
                ", quantity=" + quantity +
                '}';
    }
}
