package dev.kota.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "guitar")
public class Guitar implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String maker;
    private String name;
    private double price;
    private String condition;

    public Guitar() {
        super();
    }
    public Guitar(int id) {
        this.id = id;
    }
    public Guitar(int id, String maker, String name, double price, String condition) {
        super();
        this.id = id;
        this.maker = maker;
        this.name = name;
        this.price = price;
        this.condition = condition;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guitar guitar = (Guitar) o;
        return id == guitar.id && Double.compare(guitar.price, price) == 0 && Objects.equals(maker, guitar.maker) && Objects.equals(name, guitar.name) && Objects.equals(condition, guitar.condition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, maker, name, price, condition);
    }

    @Override
    public String toString() {
        return "Guitar{" +
                "id=" + id +
                ", maker='" + maker + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", condition='" + condition + '\'' +
                '}';
    }
}
