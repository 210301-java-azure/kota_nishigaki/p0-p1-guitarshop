package dev.kota.models;

import java.io.Serializable;
import java.util.Objects;

public class InvoiceItemId implements Serializable {

    private Invoice invoice;
    private Guitar guitar;

    public InvoiceItemId(){super();}

    public InvoiceItemId(Invoice invoice, Guitar guitar) {
        this.invoice = invoice;
        this.guitar = guitar;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Guitar getGuitar() {
        return guitar;
    }

    public void setGuitar(Guitar guitar) {
        this.guitar = guitar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvoiceItemId that = (InvoiceItemId) o;
        return Objects.equals(invoice, that.invoice) && Objects.equals(guitar, that.guitar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoice, guitar);
    }

    @Override
    public String toString() {
        return "InvoiceItemId{" +
                "invoice=" + invoice +
                ", guitar=" + guitar +
                '}';
    }
}
