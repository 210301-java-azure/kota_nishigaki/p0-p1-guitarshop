package dev.kota.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "invoice_item")
@IdClass(InvoiceItemId.class)
public class InvoiceItem {

    @Id
    @ManyToOne
    @JoinColumn(name = "invoice_id", referencedColumnName = "id")
    private Invoice invoice;

    @Id
    @ManyToOne
    @JoinColumn(name = "guitar_id", referencedColumnName = "id")
    private Guitar guitar;

    private int quantity;

    public InvoiceItem() {super();}

    public InvoiceItem(Invoice invoice, Guitar guitar, int quantity) {
        this.invoice = invoice;
        this.guitar = guitar;
        this.quantity = quantity;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Guitar getGuitar() {
        return guitar;
    }

    public void setGuitar(Guitar guitar) {
        this.guitar = guitar;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvoiceItem that = (InvoiceItem) o;
        return quantity == that.quantity && Objects.equals(invoice, that.invoice) && Objects.equals(guitar, that.guitar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoice, guitar, quantity);
    }

    @Override
    public String toString() {
        return "InvoiceItem{" +
                "invoice=" + invoice +
                ", guitar=" + guitar +
                ", quantity=" + quantity +
                '}';
    }
}
