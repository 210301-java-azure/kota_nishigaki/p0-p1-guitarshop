package dev.kota.models;


import java.io.Serializable;
import java.util.Objects;

public class CartId implements Serializable {

    private User user;
    private Guitar guitar;

    public CartId() {super();}

    public CartId(User user, Guitar guitar) {
        this.user = user;
        this.guitar = guitar;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Guitar getGuitar() {
        return guitar;
    }

    public void setGuitar(Guitar guitar) {
        this.guitar = guitar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartId cartId = (CartId) o;
        return Objects.equals(user, cartId.user) && Objects.equals(guitar, cartId.guitar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, guitar);
    }

    @Override
    public String toString() {
        return "CartId{" +
                "user=" + user +
                ", guitar=" + guitar +
                '}';
    }
}
