package dev.kota.data;

import dev.kota.models.Guitar;
import dev.kota.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class GuitarDaoHibImpl implements GuitarDao {
    @Override
    public List<Guitar> getAllGuitars() {
        try(Session s = HibernateUtil.getSession()) {
            return s.createQuery("from Guitar", Guitar.class).list();
        }
    }

    @Override
    public Guitar getGuitarById(int id) {
        try(Session s = HibernateUtil.getSession()) {
            return s.get(Guitar.class, id);
        }
    }

    @Override
    public int addNewGuitar(Guitar newGuitar) {
        try(Session s = HibernateUtil.getSession()) {
            return (int)s.save(newGuitar);
        } catch (HibernateException e) {

        }
        return 0;
    }

    @Override
    public int updateGuitarById(int id, Guitar updateGuitar) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            updateGuitar.setId(id);
            s.update(updateGuitar);
            tx.commit();
            return id;
        } catch (HibernateException e) {

        }
        return 0;
    }

    @Override
    public int deleteGuitarById(int id) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.delete(new Guitar(id));
            tx.commit();
            return id;
        } catch (HibernateException e) {

        }
        return 0;
    }
}
