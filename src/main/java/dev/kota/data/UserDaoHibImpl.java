package dev.kota.data;

import dev.kota.models.User;
import dev.kota.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class UserDaoHibImpl implements UserDao {

    private User getUserByEmail(String email) {
        try(Session s = HibernateUtil.getSession()) {
            Query<User> userQuery = s.createQuery("from User where email = :email");
            userQuery.setParameter("email", email);
            List<User> returnedUserList = userQuery.list();
            if (returnedUserList.size() == 1) return returnedUserList.get(0);
            else return null;
        }
    }

    @Override
    public User loginUser(String email, String password) {
        try(Session s = HibernateUtil.getSession()) {
            Query<User> userQuery = s.createQuery("from User where email = :email and password = :password", User.class);
            userQuery.setParameter("email", email);
            userQuery.setParameter("password", password);

            List<User> returnedUserList = userQuery.list();
            if (returnedUserList.size() == 1) return returnedUserList.get(0);
            else return null;
        }
    }

    @Override
    public int signUp(String email, String password) {

        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            User newUser = new User(0, email, password, false);

            if (getUserByEmail(email) == null) {
                int id = (int) s.save(newUser);
                newUser.setId(id);
                return id;
            }

            tx.commit();
        }
        return 0;
    }
}
