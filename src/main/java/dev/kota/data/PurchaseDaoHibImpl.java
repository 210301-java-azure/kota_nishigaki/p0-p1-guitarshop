package dev.kota.data;

import dev.kota.models.*;
import dev.kota.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class PurchaseDaoHibImpl implements PurchaseDao {
    @Override
    public List<CartItem> getCart(int userId) {
        try(Session s = HibernateUtil.getSession()) {
            String sql = "select g.id, g.name, g.price, c.quantity from guitar g join cart c on c.guitar_id = g.id where c.user_id = ?";
            List<CartItem> cartItems = s.createNativeQuery(sql).setParameter(1, userId).list();
            if (cartItems.size() > 0) return cartItems;
            else return new ArrayList<>();
        }

    }

    @Override
    public int addToCart(int userId, int itemId, int quantity) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();

            User user = s.get(User.class, userId);
            Guitar guitar = s.get(Guitar.class, itemId);
            CartId cartId = new CartId(user, guitar);

            Cart cartRow = s.get(Cart.class, cartId);

            if (cartRow != null) {
                String sql = "update cart set quantity = (select quantity from cart where user_id = ? and guitar_id = ?) + ? where user_id = ? and guitar_id = ?";
                int affectedRows = s.createNativeQuery(sql)
                        .setParameter(1, userId)
                        .setParameter(2, itemId)
                        .setParameter(3, quantity)
                        .setParameter(4, userId)
                        .setParameter(5, itemId).executeUpdate();

                tx.commit();
                return affectedRows;

            } else {
                String sql = "insert into cart (user_id, guitar_id, quantity) values (?, ?, ?)";
                int affectedRows = s.createNativeQuery(sql)
                        .setParameter(1, userId)
                        .setParameter(2, itemId)
                        .setParameter(3, quantity).executeUpdate();

                tx.commit();
                return affectedRows;
            }
        }
    }

    @Override
    public int addToCart(int userId, int itemId) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();

            User user = s.get(User.class, userId);
            Guitar guitar = s.get(Guitar.class, itemId);
            CartId cartId = new CartId(user, guitar);

            Cart itemRow = s.get(Cart.class, cartId);

            if (itemRow != null) {
                String sql = "update cart set quantity = (select quantity from cart where user_id = ? and guitar_id = ?) + 1 where user_id = ? and guitar_id = ?";
                int affectedRows = s.createNativeQuery(sql)
                        .setParameter(1, userId)
                        .setParameter(2, itemId)
                        .setParameter(3, userId)
                        .setParameter(4, itemId).executeUpdate();

                tx.commit();
                return affectedRows;

            } else {
//                Why not working...? ->
//                User user = s.get(User.class, userId);
//                Guitar guitar = s.get(Guitar.class, itemId);
//                Cart newCartEntry = new Cart(user, guitar, 1);
//                CartId id = (CartId) s.save(newCartEntry);

                String sql = "insert into cart (user_id, guitar_id, quantity) values (?, ?, ?)";
                int affectedRows = s.createNativeQuery(sql)
                        .setParameter(1, userId)
                        .setParameter(2, itemId)
                        .setParameter(3, 1).executeUpdate();

                tx.commit();
                return affectedRows;
            }
        }
    }

    @Override
    public int deleteFromCart(int userId, int itemId) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();

            User user = s.get(User.class, userId);
            Guitar guitar = s.get(Guitar.class, itemId);
            CartId cartId = new CartId(user, guitar);

            Cart cartRow = s.get(Cart.class, cartId);

            if (cartRow != null) {

                s.delete(cartRow);
                tx.commit();
                return 1;

            } else return 0;
        }
    }

    @Override
    public int purchase(int userId) {

            try(Session s = HibernateUtil.getSession()) {
                Transaction tx = s.beginTransaction();

                List<Cart> cartRows = s.createQuery("from Cart where user_id = :user_id", Cart.class)
                        .setParameter("user_id", userId).list();

                if (cartRows.isEmpty()) return 0;

                User user = s.get(User.class, userId);

                Invoice invoice = new Invoice(user);
                invoice.setId((int)s.save(invoice));

                for (int i = 0; i < cartRows.size(); i++) {

                    Guitar guitar = cartRows.get(i).getGuitar();
                    int quantity = cartRows.get(i).getQuantity();

                    InvoiceItem invoiceItemRow = new InvoiceItem(invoice, guitar, quantity);
                    s.save(invoiceItemRow);

                    s.delete(cartRows.get(i));

                }

                tx.commit();
                return 1;
            }
    }
}
