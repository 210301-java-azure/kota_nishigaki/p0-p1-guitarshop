FROM java:8
COPY build/libs/guitar-shop-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar guitar-shop-1.0-SNAPSHOT.jar
